//variant: 13 mod 8 + 1 = 6

var fr = new FileReader();
var fileData = '';

console.log(getKeyLfsr('1111', new Array(3,0)));


$(document).ready(function() {
    $('input[type=file]').change(function(data){
        uploadFile();
    });
    $('.lfsr1_1').keydown(function(event) {
        binaryInput(event);     
    });
    $('#shifr1').click(function() {        
        shifr1();
    });
    $('#shifr2').click(function(){
        shifr2();
    });
    $('#shifr3').click(function() {
        shifr3();
    });
    $('.default1').click(function() {
        default1();
    });
    $('.default2').click(function() {
        default2();
    });
    $('.default3').click(function() {
        default3();
    });
    $('.allShifr').click(function() {
        if(shifr1())
            if(shifr2())
                shifr3();
            
    });
    $('.key3').keydown(function(evetnt) {
        digitInput(event);
    });
    $('.allDefault').click(function() {
        default1();
        default2();
        default3();
    });
});

function shifr1() {
    let data = '';
    data = $('.input1').val();
    if(data == '') {
        alert('Введите входные байты, либо загрузите файл');
        return false;
    }
    let lef1 = $('.lfsr1_1').val();
    if(lef1.length != 28 ) {
        alert('Начальное значение для lsfr1 должно быть 28 бит');
        return false;
    }
    lfsr1 = bitsToNum(lef1);

    let key = '';
    let codedData = '';
    let decodedData = '';
    for(let i = 0; i<data.length; ++i) {
        let bits = '';
        for(let j=0;j<8;++j) {
            bits += getBit((lfsr1 << 1), 28);
            lfsr1 = lfsrStep(lfsr1, lef1.length, new Array(27,2));
        }
        key += bits;
        let bitsNum = bitsToNum(bits);
        let nextCode = String.fromCharCode(bitsNum ^ data.charCodeAt(i));
        codedData += nextCode;
        decodedData += String.fromCharCode(bitsNum ^ nextCode.charCodeAt(0));
        $('.key1').val(key);
        $('.output1').val(codedData);
        $('.outputdec1').val(decodedData);
    }   
    return true;
}

function shifr2() {
    let lef1, lef2, lef3, lfsr1, lfsr2, lfsr21, lfsr3;
    let data = '';
    data = $('.input2').val();
    if(data == '') {
        alert('Введите входные байты, либо загрузите файл');
        return false;
    }
    lef1 = $('.lfsr2_1').val();
    if(lef1.length != 28 ) {
        alert('Начальное значение для lsfr1 должно быть 28 бит');
        return false;
    }
    lef2 = $('.lfsr2_2').val();
    if(lef2.length != 36 ) {
        alert('Начальное значение для lsfr1 должно быть 28 бит');
        return false;
    }
    lef3 = $('.lfsr2_3').val();
    if(lef3.length != 26 ) {
        alert('Начальное значение для lsfr1 должно быть 28 бит');
        return false;
    }
    lfsr1 = bitsToNum(lef1);
    let lef21 = '';
    let lef22 = '';
    let k = 0;
    for(k = 0; k < 31; ++k) lef21 += lef2[k];
    for(;k<36;++k) lef22 += lef2[k];

    lfsr21 = bitsToNum(lef21);
    lfsr22 = bitsToNum(lef22);    

    lfsr3 = bitsToNum(lef3);

    let key1 = '', key2 = '', key3 = '';
    let key = '';
    let codedData = '';
    let decodedData = '';
    for(let i = 0; i<data.length; ++i) {
        let bits = '';
        for(let j=0;j<8;++j) {
            let x1,x2,x3,xg, tmp;
            x1 = getBit((lfsr1 << 1), 28);
            key1 += x1;
            lfsr1 = lfsrStep(lfsr1, lef1.length, new Array(27,2));
            x2 = getBit((lfsr21 << 1), 31);
            key2 += x2;
            tmp = getBit((lfsr22 << 1), 5);            
            lfsr21 = zeroBit((lfsr21 << 1), 31) + tmp;
            lfsr22 = zeroBit((lfsr22 << 1), 5) + xor(x2, getBit(lfsr21, 5));
            x3 = getBit((lfsr3 << 1), 26);
            key3 += x3;
            lfsr3 = lfsrStep(lfsr3, lef3.length, new Array(25,7,6,0));

            xg = or(and(x1,x2), (and(not(x1),x3)));
            bits += xg;
        }
        
        key += bits;
        let bitsNum = bitsToNum(bits);
        codedData += String.fromCharCode(bitsNum ^ data.charCodeAt(i));
        decodedData += String.fromCharCode(bitsNum ^ codedData.charCodeAt(i));
        
    }   
    $('.output2').val(convertToBinary(codedData));
        $('.inputbit2').val(convertToBinary(data));
        $('.outputdec2').val(decodedData);
        $('.key2').val(key);
        $('.key2_1').val(key1);
        $('.key2_2').val(key2);
        $('.key2_3').val(key3);
    return true;
}

function shifr3() {
    let data = '';
    data = $('.input3').val();
    if(data == '') {
        alert('Введите входные байты, либо загрузите файл');
        return false;
    }
    let str = $('.key3').val();
    if(str == '') {
        alert('Введите ключ для RC4');
        return false;
    }
    let key = str.trim().replace(/\s+/g,' ').split(' ').map(num => num - 0);
    let s = Array(256);
    let thread = Array();
    let outKey = '';
    let decodedData = '';
    let codedData = '';
    for(let i = 0; i<256; ++i) {
        s[i] = i;
    }
    for(let i = 0; i<256;++i) {
        for(let j = 0; j<256; ++j) {
            j = (j + s[i] + key[i % key.length]) % 256;
            swap(s,i,j);
        }
    }
    i = 0;
    j = 0;
    function getNextThread() {
        i = (i + 1) % 256;
        j = (j + s[i]) % 256;
        swap(s,i,j);
        return s[(s[i] + s[j]) % 256];
    }

    for(let i = 0; i<data.length; ++i) {
        let k = getNextThread();
        outKey += ' ' + k;
        let nextCode = String.fromCharCode(k ^ data.charCodeAt(i)); 
        codedData += nextCode;
        decodedData += String.fromCharCode(k ^ nextCode.charCodeAt(0));
    }
    $('.outKey3').val(outKey);
    $('.output3').val(codedData);
    $('.outputdec3').val(decodedData); 

}

function swap(s, i,j) {
    let t = s[i];
    s[i] = s[j];
    s[j] = t;
}
function getKeyLfsr(lfsrStr, arr) {
     let key = '';
     let length = lfsrStr.length;
     let lfsr = bitsToNum(lfsrStr);
     let startValue = lfsr;
     do {
        key += getBit((lfsr << 1), length);
        lfsr = lfsrStep(lfsr, length, arr);
    }while(lfsr != startValue);
    key += getBit((lfsr << 1), length);    
    return key;
}

function receive() {
    $('.input').val(fr.result);
    fileData = fr.result;
}

function convertToBinary(str) {
    res = '';
    for(let i = 0; i<str.length; ++i) {
        res += ("00000000" + str[i].charCodeAt(0).toString(2)).substr(-8).split("").reverse().join("");
    }
    return res;
}

function uploadFile() {
    let input = document.getElementById('up');
    let f = input.files[0];
    fr.onload = receive;
    fr.readAsDataURL(f);
}

function binaryInput(event) {
    if (event.key != '0' && event.key != '1' && event.key != 'Backspace') {
        event.preventDefault();   
        return;
    }  
}

function digitInput(event) {
    if((event.key < '0' || event.key >'9') && event.key != ' ' && event.key != 'Backspace') {
        console.log(event.key);
        event.preventDefault();
        return;
    }
}

function lfsrStep(lfsr, pos, arr) {
        return zeroBit((lfsr << 1),pos) + getNextLfsr(lfsr, arr);    
}


function testCase() {
    lfsr1 = bitsToNum("1111");
    let startValue=lfsr1;
    let key = '';
    console.log('states from 1111');
    console.log('1111');
    do {
        let state = '';
        key += getBit((lfsr1 << 1), 4);
        lfsr1 = lfsrStep(lfsr1, 4, new Array(3,0));
        for(let i = 3; i>=0;--i) {
            state += getBit(lfsr1, i);
        }
        console.log(state);
    }while(lfsr1 != startValue);
    key += getBit((lfsr1 << 1), 4);    
    console.log('KEY: ' + key);
}

function zeroBit(num, pos) {
    return num & (~(1 << pos));
}

function getBit(num, pos) {
    return ((1 << pos) & num) == 0 ? 0 : 1;
}

function getNextLfsr(lef, arr) {
    res = 0;
    arr.forEach(function(val, i, arr) {
        res = xor(res, getBit(lef, val)); //0 0 = 0, 1 1 = 0 
    });
    return res;
}

function xor(i, j) {
    return i == j ? 0 : 1;
}
function and(i,j) {
    return (i == 1) && (j == 1) ? 1 : 0;
}
function or(i,j) {
    return (i == 1) || (j == 1) ? 1 : 0;
}
function not(i) {
    return i == 1 ? 0 : 1;
}

function bitsToNum(str) {
    let res = 0;
    str = str.split("").reverse().join("");
    for(let i = 0; i<str.length; ++i) {
        res += str[i] == '0' ? 0 << i : 1 << i;
    }
    return res;
}

function default1() {
    let res = '';
    for(let i = 0; i<28; ++i) {
        res += '1';
    }
    $('.lfsr1_1').val(res);
}

function default2() {
    let res = '';
    for(let i = 0; i<28; ++i) res += '1';
    $('.lfsr2_1').val(res);
    res = '';
    for(let i = 0; i<36; ++i) res += '1';
    $('.lfsr2_2').val(res);    
    res = '';
    for(let i = 0; i<26; ++i) res += '1';
    $('.lfsr2_3').val(res);
}

function default3() {
    $('.key3').text('10 20 30 40 23 45 11');
}