var simpleDigits = new Array(10000);

$(document).ready(function() {
    $('.count').click(function(){
        count();
    });
    $('.inputP, .inputQ').keydown(function(e) {
        digitInput(e);
    });
    initPrimes();  
    console.log(getE(3,10));
    
});


function count() {
    let msg, msgHash10, msgHash16;
    msg = $('.inputMessage').val();
    msgHash10 = Sha1.hash(msg, {outFormat: "dec"});
    msgHash16 = Sha1.hash(msg);
    $('.outputHash10').val(msgHash10);
    $('.outputHash16').val(msgHash16);

    let p,q,n, d, e, ck, ok;
    p = $('.inputP').val();
    q = $('.inputQ').val();
    if(!isPrime(p)) {
        alert('P должно быть простым');
    }
    if(!isPrime(q)) {
        alert('Q должно быть простым');
    }

    n = p*q; 
    m = (p - 1) * (q - 1);
    for(let i = 2; i<m;++i) {
        if(nod(m,i)==1) {
            d = i;
            break;
        }
    }    
    e = getE(m,d);
    ok = d;
    ck = e;
    $('.inputCloseKey').val(ck);
    $('.outputOpenKey').val(ok);

    let ecp, decEcp, dijest;
    ecp = new Array();
    msgHash10 += '';
    for(let i= 0;i<msgHash10.length; ++i) {
        ecp.push(Math.pow(msgHash10[i] - 0, ck) % n);
    }
    outArray(ecp,'.outputECP');

    decEcp = new Array();
    for(let i = 0; i<ecp.length; ++i) {
        decEcp.push(Math.pow(ecp[i], ok) % n);
    }
    outArray(decEcp, '.outputCheckECP');
}

function outArray(arr, selector) {
    let res = ''
    arr.forEach(function(el) {
        res += el + '';
    });
    $(selector).val(res);
}

function digitInput(event) {
    if((event.key < '0' || event.key >'9') && event.key != ' ' && event.key != 'Backspace' && event.key!='Tab') {
        console.log(event.key);
        event.preventDefault();
        return;
    }
}

class Sha1 {
    static hash(msg, options) {
        const defaults = { msgFormat: 'string', outFormat: 'hex' };
        const opt = Object.assign(defaults, options);

        switch (opt.msgFormat) {
            default: 
            case 'string':   msg = utf8Encode(msg);       break;
            case 'hex-bytes':msg = hexBytesToString(msg); break; 
        }

        const K = [ 0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6 ];

        const H = [ 0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0 ];


        msg += String.fromCharCode(0x80);

        const l = msg.length/4 + 2; 
        const N = Math.ceil(l/16);  
        const M = new Array(N);
        for (let i=0; i<N; i++) {
            M[i] = new Array(16);
            for (let j=0; j<16; j++) {  
                M[i][j] = (msg.charCodeAt(i*64+j*4+0)<<24) | (msg.charCodeAt(i*64+j*4+1)<<16)
                        | (msg.charCodeAt(i*64+j*4+2)<< 8) | (msg.charCodeAt(i*64+j*4+3)<< 0);
            } 
        }
        M[N-1][14] = ((msg.length-1)*8) / Math.pow(2, 32); M[N-1][14] = Math.floor(M[N-1][14]);
        M[N-1][15] = ((msg.length-1)*8) & 0xffffffff;

        for (let i=0; i<N; i++) {
            const W = new Array(80);

            for (let t=0;  t<16; t++) W[t] = M[i][t];
            for (let t=16; t<80; t++) W[t] = Sha1.ROTL(W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16], 1);

            let a = H[0], b = H[1], c = H[2], d = H[3], e = H[4];

            for (let t=0; t<80; t++) {
                const s = Math.floor(t/20); // seq for blocks of 'f' functions and 'K' constants
                const T = (Sha1.ROTL(a,5) + Sha1.f(s,b,c,d) + e + K[s] + W[t]) >>> 0;
                e = d;
                d = c;
                c = Sha1.ROTL(b, 30) >>> 0;
                b = a;
                a = T;
            }

            H[0] = (H[0]+a) >>> 0;
            H[1] = (H[1]+b) >>> 0;
            H[2] = (H[2]+c) >>> 0;
            H[3] = (H[3]+d) >>> 0;
            H[4] = (H[4]+e) >>> 0;
        }

        if(opt.outFormat == 'dec') 
            for(let h = 0; h<H.length; h++) H[h] = H[h].toString(10);
        else
            for (let h=0; h<H.length; h++) H[h] = ('00000000'+H[h].toString(16)).slice(-8);

        const separator = opt.outFormat=='hex-w' ? ' ' : '';

        return H.join(separator);


        function utf8Encode(str) {
            try {
                return new TextEncoder().encode(str, 'utf-8').reduce((prev, curr) => prev + String.fromCharCode(curr), '');
            } catch (e) { 
                return unescape(encodeURIComponent(str));
            }
        }

        function hexBytesToString(hexStr) { 
            const str = hexStr.replace(' ', '');
            return str=='' ? '' : str.match(/.{2}/g).map(byte => String.fromCharCode(parseInt(byte, 16))).join('');
        }
    }


    static f(s, x, y, z)  {
        switch (s) {
            case 0: return (x & y) ^ (~x & z);          
            case 1: return  x ^ y  ^  z;                
            case 2: return (x & y) ^ (x & z) ^ (y & z); 
            case 3: return  x ^ y  ^  z;                
        }
    }


    static ROTL(x, n) {
        return (x<<n) | (x>>>(32-n));
    }
}

function initPrimes() {
    for(let i = 0; i<simpleDigits.length; ++i) {
        simpleDigits[i] = true;
    }
    simpleDigits[0] = false;
    simpleDigits[1] = false;
    for(let i = 2; i*i<simpleDigits.length; ++i) {
        if(simpleDigits[i]) {
            for(let j = i*i; j*j<simpleDigits.length; j+=i) {
                simpleDigits[j] = false;
            }
        }
    }
}

function isPrime(i) {
    return simpleDigits[i];
}

function nod(a,b) {
    let r = a % b;
    if(r == 0)  return b;
    return nod(b,r);
}

var earr = [[1,0],[0,1]];
var mnarr = [[0,1],[1,1]];
function getE(m,d) {
    for(let i = 0;i<m;++i) {
        if((i*d)%m == 1) {
            return i;
        }
    }
    return -1;
}

function div(a,b) {
    return(a - a%b) /b;
}